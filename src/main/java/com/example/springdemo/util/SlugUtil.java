package com.example.springdemo.util;

public class SlugUtil {
    public static String toSlug(String string) {
        return String.join("-",
                string.toLowerCase()
                .replace("\n", " ")
                .replaceAll("[^a-z\\d\\s]", " ")
                .split(" "))
                .replaceAll("-+", "-");
    }
}
