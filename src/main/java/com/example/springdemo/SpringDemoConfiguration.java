package com.example.springdemo;

import com.example.springdemo.domain.Article;
import com.example.springdemo.domain.User;
import com.example.springdemo.repository.ArticleRepository;
import com.example.springdemo.repository.UserRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDemoConfiguration {

    @Bean
    public ApplicationRunner databaseInitializer(UserRepository userRepository, ArticleRepository articleRepository) {
        return args -> {
            User smaldini = userRepository.save(new User("smaldini", "Stéphane", "Maldini"));
            articleRepository
                    .save(new Article("Reactor Bismuth is out", "Lorem ipsum", "dolor sit amet", smaldini));
            articleRepository
                    .save(new Article("Reactor Aluminium has landed", "Lorem ipsum", "dolor sit amet", smaldini));
        };
    }
}
