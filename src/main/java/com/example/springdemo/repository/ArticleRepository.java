package com.example.springdemo.repository;

import com.example.springdemo.domain.Article;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Long> {
    Article findBySlug(String slug);
    List<Article> findAllByOrderByAddedAtDesc();
}
